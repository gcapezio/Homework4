
public class LinkedBagTester { //tester
public static void main(String[] args){
	LinkedBag<String> b= new LinkedBag<String>(); //1
	b.add("A");
	b.add("B");
	b.add("C");
	b.add("D");
	System.out.println(b.contains("D"));
	b.remove("D");
	System.out.println(b.contains("D"));
	ArrayBag<String> c= new ArrayBag<String>();//2
	c.add("L");
	c.add("M");
	c.add("N");
	c.add("O");
	System.out.println(c.contains("L"));
	LinkedBag<String> d= new SpecialLinkedBag<String>(); //3
	d.add("1");
	d.add("2");
	d.add("3");
	System.out.println(d.contains("6"));
	d.add("6");
	System.out.println(d.contains("6"));
	Collection<String> e= new ArrayBag<String>(); //4
	e.add("11");
	e.add("12");
	e.add("13");
	System.out.println(e.contains("11"));
	Collection<String> f= new LinkedBag<String>(); //5
	f.add("atticus");
	f.add("monica");
	f.add("fishtopher");
	System.out.println(f.contains("carol"));
	Collection<String> g = new SpecialLinkedBag<String>(); //6
	g.add("peanut butter");
	g.add("and");
	g.add("jelly");
	g.add("time");
	System.out.println(g.contains("time"));
	SpecialLinkedBag<String> h= new SpecialLinkedBag<String>(); //7
	String[] p = {"3","5","6"};
	h.addAll(p);
	System.out.println(h.contains("1")); //three ways to implement the special linked bag
									//from an instantiation of itself, of collection and of linked bag

	
	
	
	
}
}
