import java.util.*;
public class LinkedBag<T> implements Collection<T> {
	private int n; //n is number of elements in bag
	private LinkedNode<T> first; //first element 
	
	public LinkedBag(){ //constructor
		first = null; //empty bag
		n=0; //no elements
	}
	
	public void add(T item){ //runtime of O(1)
		LinkedNode<T> node= new LinkedNode<T>(item);
		node.setNext(first); //new first
		first=node; 
		n++;
	}
	public boolean contains(T item){ //runtime O(n)
		LinkedNode<T> current= first; 
		while (current !=null){ 
			if (current.getItem().equals(item)) //checks to see if current equals the item
			return true; //true if they are equal
			else
				current=current.getNext(); //keep checking all n items if not 
		}
		return false; }
		
		
	public void remove(T item){
		LinkedNode<T> current;
		if (first == null){ //nothing to remove
			return; }
		else if (first.getItem().equals(item)){ //first item is the one we want to remove
			first=first.getNext(); //new first
			n--; } //one less element 

		else{
			current= first;
			while (current.getNext()!=null){ //while there are items to be removed
				if ((current.getNext()).getItem().equals(item)){ //current is equal to item being removed
					((LinkedNode<T>) item).setItem(current.getItem()); //casting, setting current=item, removing item
					current=current.getNext(); //new current
					n--;
				}
				else{
					current= current.getNext(); } //new current so we can go through again until current = item
					 }
				}
			}
	}




