
public class Point {
		private int x;
		private int y;
		
		// Constructor
		public Point(int newx, int newy){
			x = newx;
			y = newy;
		}
		public String toString(){
			return "("+x+","+y+")";
		}
		public int getX(){
			return x;
		}
		
		public int getY(){
			return y;
		}
		public void setX(int newX){
			x=newX;
		}
		
		public void setY(int newY){
			y=newY;
		}
		@Override //overriding equals
		public boolean equals(Object o) {
		    if (o == null) { //must have a value
		        return false;
		    }
		    if (!(o instanceof Point)) { //cannot be an instance
		        return false;
		    }
		    return (x == ((Point) o).x && y == ((Point) o).y); //casting and checking if x and y are equal
	}
	
	}

