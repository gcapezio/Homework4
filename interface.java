
public interface Collection<T> {
	
    public void add(T item);

    public void remove(T item);

    public boolean contains(T item);

}

