
public class LinkedNode<T> {
	private LinkedNode<T> next;
	private T data;
	public LinkedNode(){ //constructor
		data= null;
		next = null;
	}
	public LinkedNode(T x){ //single node
		next=null;
		data=x;
		
	}
	public void setNext(LinkedNode<T> node){
		next=node;
	}
	public void setItem(T y){
		data=y;
	}
	public LinkedNode<T> getNext(){
		return next;
	}
	public T getItem(){
		return data;
	}
}
